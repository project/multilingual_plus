<?php

namespace Drupal\multilingual_plus\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\text\Plugin\Field\FieldFormatter\TextDefaultFormatter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Allow showing the translation of a text.
 *
 * When the text does not have translation, there
 * is an explanation saying 'No text available'.
 *
 * @FieldFormatter(
 *   id = "multilingual_plus_translated_text",
 *   label = @Translation("Translated text"),
 *   field_types = {
 *     "text",
 *     "text_long"
 *   }
 * )
 */
class TranslatedTextFormatter extends TextDefaultFormatter {

  use StringTranslationTrait;

  /**
   * Entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected EntityRepositoryInterface $entityRepository;

  /**
   * Language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityRepository = $container->get('entity.repository');
    $instance->languageManager = $container->get('language_manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $entity = $items->getEntity();
    $entity_from_context = $this->entityRepository->getTranslationFromContext($entity);
    if ($entity_from_context->language()->getId() == $this->languageManager->getCurrentLanguage()->getId()) {
      $items = $entity_from_context->get($items->getName());
      return parent::viewElements($items, $langcode);
    }
    else {
      $elements = [];

      // The ProcessedText element already handles cache context & tag bubbling.
      // @see \Drupal\filter\Element\ProcessedText::preRenderText()
      foreach ($items as $delta => $item) {
        $elements[$delta] = [
          '#type' => 'container',
          'fallback_text' => [
            '#type' => 'html_tag',
            '#tag' => 'div',
            '#value' => $this->t('No translation available.'),
          ],
          'value' => [
            '#type' => 'processed_text',
            '#text' => $item->value,
            '#format' => $item->format,
            '#langcode' => $item->getLangcode(),
          ],
        ];
      }
      return $elements;
    }
  }

}
